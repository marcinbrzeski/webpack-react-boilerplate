// - one can use:
// $ npm i npm-install-webpack-plugin --save-dev
// const NpmInstallPlugin = require('npm-install-webpack-plugin');
// - and than
// new webpack.HotModuleReplacementPlugin(),
// new NpmInstallPlugin({
//     save: true // --save
// })
// - in plugins to automatically install all needed modules

const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge'); // smarter merge without overriding
const SCRIPT = process.env.npm_lifecycle_event;
process.env.BABEL_ENV = SCRIPT; // tell babel what we are doing, so we can control .babelrc execution

const PATHS = {
    app: path.join(__dirname, 'app'),
    build: path.join(__dirname, 'build')
};

const common = {
    entry: { // where to find original files:
        app: PATHS.app
    },
    output: { // where to put build result and how to name it:
        path: PATHS.build,
        filename: 'bundle.js'
    },
    module: { // lets load some modules to process files
        loaders: [ // loaders are processed form bottom right to top left
            {
                // $ npm i css-loader style-loader -D
                test: /\.css$/, // for all files that matches this regexp
                loaders: ['style', 'css'], // use this loaders, 'css' first, than 'style'
                // 'css' will resolve @import and url statements in css files
                // 'style' will resolve require for css in .js files
                include: PATHS.app // where to lok for files; one can also use 'exclude'
            },
            {
                // now babel, remember to add presets; babel 6 have everything turned off by default
                // $ npm i babel-preset-{es2015,react} -D
                // one can set .babelrc or add presets as parameters
                test: /.jsx?$/, // will take care of .js and .jsx; one can also add resolve:{ extensions: ['', '.js', '.jsx']},- not to have to specify file extensions; let's keep it more explicit and provde them
                loader: 'babel', // you can add cache direcotry for babel
                query: {
                    cacheDirectory: '_babel_cache',
                    // presets: ['react','es2015'] // sems not to be working :( - can't find
                },
                inlcude: PATHS.app
                // we also need somthing to swap react modules ont he fly
                // $ npm i babel-preset-react-hmre --save-dev
            }
        ]

    }
};

console.log('>>>>> Preparing config for: \'' + process.env.npm_lifecycle_event + '\'\n');

if (SCRIPT==='start' || !SCRIPT){
    module.exports = merge( common, {
        devtool: 'eval-source-map',
        // all of it for HMR
        devServer: {
            contentBase: PATHS.build, // webpack --content-base build <- not needed anymore in package.json
            historyApiFallback: true, // helps with HMR plugin - how?
            hot: true, // HMR?
            inline: true, // will generate frontend scripts for HMR?
            progress: true,
            stats: 'errors-only',
            host: process.env.HOST,
            port: process.env.PORT
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin()
        ]
    });
}
if (SCRIPT === 'build'){
    module.exports = Object.assign({}, common, {

    });
}
